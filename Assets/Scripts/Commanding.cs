﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Valve.VR.InteractionSystem
{
	public class Commanding : MonoBehaviour
	{

	    private SteamVR_TrackedObject trackedObj;

	    // This is a reference to the Laser’s prefab.
	    public GameObject laserPrefab;

	    // laser stores a reference to an instance of the laser.
	    private GameObject laser;

	    // The transform component is stored for ease of use.
	    private Transform laserTransform;

	    // This is the position where the laser hits.
	    private Vector3 hitPoint;

	    //This has been moved for the sake of killing enemies
	    RaycastHit hit;

	    //-------------------------------------------
	    //ADDITIONS FROM PLAYER SHOOTING
	    public int damagePerShot = 20;
	    public float timeBetweenBullets = 0.15f;
	    public float range = 100f;

	    public LayerMask robotMask;
		public LayerMask floorMask;

		private Hand hand;
		public Hand hand1;
		public Hand hand2;

		bool selectRobot;

	    float timer;
	    Ray shootRay;
	    RaycastHit shootHit;
	    
	    ParticleSystem gunParticles;
	    LineRenderer gunLine;
	    AudioSource gunAudio;
	    Light gunLight;
	    float effectsDisplayTime = 0.2f;

	    //------------------------------------------

	    void Awake()
	    {
	        trackedObj = GetComponent<SteamVR_TrackedObject>();

	        //-------------------------------------------------
	        gunParticles = GetComponent<ParticleSystem>();
	        gunLine = GetComponent<LineRenderer>();
	        gunAudio = GetComponent<AudioSource>();
	        gunLight = GetComponent<Light>();
	        //------------------------------------------------
	    }

	    // This method takes a RaycastHit as a parameter because it contains 
	    // the position of the hit and the distance it traveled.
	    private void ShowLaser(RaycastHit hit)
	    {
	        // Show the laser.
	        laser.SetActive(true);

	        // Position the laser between the controller and the point where the raycast hits. 
	        // You use Lerp because you can give it two positions and the percent it should travel. 
	        // If you pass it 0.5f, which is 50%, it returns the precise middle point.
	        //laserTransform.position = Vector3.Lerp(trackedObj.transform.position, hitPoint, .5f);
			laserTransform.position = Vector3.Lerp(hand.transform.position, hitPoint, .5f);

	        // Point the laser at the position where the raycast hit.
	        laserTransform.LookAt(hitPoint);

	        // Scale the laser so it fits perfectly between the two positions.
	        laserTransform.localScale = new Vector3(laserTransform.localScale.x, laserTransform.localScale.y,
	            hit.distance);
	    }

	    void Start()
	    {

	        // Spawn a new laser and save a reference to it in laser.
	        laser = Instantiate(laserPrefab);

	        // Store the laser’s transform component.
	        laserTransform = laser.transform;

			selectRobot = false;

	    }


	    // Update is called once per frame
	    void Update()
	    {

	        timer += Time.deltaTime;

	        // If the touchpad is held down…
			if (hand1.controller.GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
				hand = hand1;
			} else if (hand2.controller.GetPress (SteamVR_Controller.ButtonMask.Trigger)) {
				hand = hand2;
			}

			if ((hand1.controller.GetPress(SteamVR_Controller.ButtonMask.Trigger) || hand2.controller.GetPress(SteamVR_Controller.ButtonMask.Trigger)) && timer >= timeBetweenBullets && Time.timeScale != 0)
			{
				shootRay.origin = hand.transform.position;
				shootRay.direction = hand.transform.forward;

	            // Shoot a ray from the controller. 
	            // If it hits something, make it store the point where it hit and show the laser.
	            if (Physics.Raycast(shootRay.origin, shootRay.direction, out hit, 100))
	            {
	                hitPoint = hit.point;
	                ShowLaser(hit);
	                Shoot(shootRay);
	            }
	        }
	        else // Hide the laser when the player released the touchpad.
	        {
	            laser.SetActive(false);

	        }

	        if (timer >= timeBetweenBullets * effectsDisplayTime)
	        {
	            DisableEffects();
	        }
	    }

	    //---------------------------------------------------------------------------------------------

	    public void DisableEffects()
	    {
	    }


	    void Shoot(Ray shootRay)
	    {
	        timer = 0f;

	        if (Physics.Raycast(shootRay.origin, shootRay.direction, out hit, 100, robotMask))
	        {
				GameObject fleet = GameObject.Find ("Fleet");

				FleetController fleetController = fleet.GetComponent<FleetController> ();

				fleetController.selectedRobot = hit.collider.gameObject;

				selectRobot = true;

	            gunLine.SetPosition(1, hit.point);
	        }
			else if ((selectRobot == true) && (Physics.Raycast(shootRay.origin, shootRay.direction, out hit, 100, floorMask)))
			{
				GameObject fleet = GameObject.Find ("Fleet");

				FleetController fleetController = fleet.GetComponent<FleetController> ();

				Robot[] robots = fleetController.robots;
				Target[] targets = fleetController.targets;

				for (int i = 0; i < targets.Length; i++) {
					if (targets [i].id == 0) {

						for (int j = 0; j < robots.Length; j++) {
							if (robots [j].model == fleetController.selectedRobot) {

								targets [i].id = robots [j].id;
								targets [i].position = hit.point;
								targets [i].rotation = robots [j].rotation;
								targets [i].time = Time.time;
								targets [i].check = 0;
							}
						}
					}
				}

				gunLine.SetPosition(1, hit.point);
			}
	        else
	        {
	            gunLine.SetPosition(1, shootRay.origin + shootRay.direction * range);
	        }
	    }
	}

    //---------------------------------------------------------------------------------------------
}

