using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.Timers;
using UnityEngine.Networking;


public class WheelMovement : MonoBehaviour 
{
	public float speed;
	public float wheelRadius;

	public Transform wheelFLTrans;
	public Transform wheelFRTrans;
	public Transform wheelRLTrans;
	public Transform wheelRRTrans;

	public Transform robotTrans;

	public Rigidbody robot;

	Vector3 iniPos, endPos;

	Quaternion iniRot, endRot;

	void Start()
	{
		robot = GetComponent<Rigidbody> ();  

		iniPos = robotTrans.position;

		iniRot = robotTrans.rotation;
	}

	void FixedUpdate() 
	{
		endPos = robotTrans.position;

		endRot = robotTrans.rotation;
					            
        float forward;
        float sideways;

		float iniAngle, endAngle;

		Vector3 axis = Vector3.up;

		iniRot.ToAngleAxis(out iniAngle, out axis);

		axis = Vector3.up;

		endRot.ToAngleAxis (out endAngle, out axis);

		if (iniRot == endRot)
        {
			// If the orientation of the robot hasn't changed

            sideways = 0;
            
			forward = Mathf.Sqrt (Mathf.Pow(endPos.x - iniPos.x ,2) + Mathf.Pow(endPos.z - iniPos.z, 2));
            
			if (((endPos.x - iniPos.x) < 0 && (Mathf.Abs(endAngle) < 1.5 || Mathf.Abs(endAngle) > 4.5))
				|| (((endPos.x - iniPos.x) > 0) && (Mathf.Abs(endAngle) > 1.5 || Mathf.Abs(endAngle) < 4.5))
				|| (((endPos.y - iniPos.y) > 0) && Mathf.Abs(endAngle) < 3)
				|| (((endPos.y - iniPos.y) > 0) && Mathf.Abs(endAngle) > 3))
            {
                forward = - forward;
            }
        }
        else
        {
			if (endAngle - iniAngle > 0) {
				sideways = 1;
			} else {
				sideways = -1;
			}

            forward = 0;
        }

        RotateWheels(forward, sideways, speed); 

		iniPos = robotTrans.position;

		iniRot = robotTrans.rotation;
    }


	void RotateWheels(float forward, float sideways, float speed)
	{
		if (sideways == 0) {

			// When we move forwards or backwards all wheels turn equally

			// Left wheels
			wheelFLTrans.Rotate (0, 0, forward * speed * 1000 / wheelRadius * Time.deltaTime);
			wheelRLTrans.Rotate (0, 0, forward * speed * 1000 / wheelRadius * Time.deltaTime);

			// Right wheels
			wheelFRTrans.Rotate (0, 0, forward * speed * 1000 / wheelRadius * Time.deltaTime);
			wheelRRTrans.Rotate (0, 0, forward * speed * 1000 / wheelRadius * Time.deltaTime);

		} else {

			// When we turn left,  left  wheels rotate backwards and sideways = -1
			// When we turn right, right wheels rotate backwards and sideways = 1

			// Left wheels
			wheelFLTrans.Rotate (0, 0, sideways * speed * 1000 / wheelRadius * Time.deltaTime);
			wheelRLTrans.Rotate (0, 0, sideways * speed * 1000 / wheelRadius * Time.deltaTime);

			// Right wheels
			wheelFRTrans.Rotate (0, 0, -sideways * speed * 1000 / wheelRadius * Time.deltaTime);
			wheelRRTrans.Rotate (0, 0, -sideways * speed * 1000 / wheelRadius * Time.deltaTime);

		}
	}
}

