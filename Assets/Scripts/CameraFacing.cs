﻿//=========================================================================================

// Class Name: CameraFacing
// Project: ROSBookWorld
// Author: Elena Peña Tapia
// Purpose: Makes the UAVs' HUD Canvas face the camera at all times.
// Script Interactions: none
// GameObject Interactions: Player, HUDCanvas
// GameObject Attached to: 

//=========================================================================================

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Valve.VR.InteractionSystem;

public class CameraFacing : MonoBehaviour 
{

	// Public variables
	// --------------------------------
	public Player player;
	// ---------------------------------

	// Private variables
	// ---------------------------------
    private Transform cameraTrans;
	// ---------------------------------
   
	void Update () {

        cameraTrans = player.hmdTransform;

        Vector3 cameraRot = cameraTrans.rotation.eulerAngles;

        Vector3 canvasRot = cameraRot - new Vector3(0,0,0);

        transform.rotation = Quaternion.Euler(canvasRot);

	}

}
