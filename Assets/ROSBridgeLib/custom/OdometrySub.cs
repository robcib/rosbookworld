﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.nav_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;
using System;

/**
 * 
 * Subscriber of Odometry message.
 * 
 * Juan Jesús Roldán: jj.roldan@upm.es
 * 
 **/

public class OdometrySub : ROSBridgeSubscriber {

	public new static string GetMessageTopic() {
		return "/odometry";
	}  

	public new static string GetMessageType() {
		return "nav_msgs/Odometry";
	}

	public new static ROSBridgeMsg ParseMessage(JSONNode msg) {
		return new OdometryMsg (msg);
	}

	public new static void CallBack(ROSBridgeMsg msg)
	{
		OdometryMsg odom = (OdometryMsg) msg;

		HeaderMsg header = odom.GetHeader ();

		string frame_id = header.GetFrameId ();

		string number_id = frame_id.Substring (4, 3);

		PoseWithCovarianceMsg pose_cov = odom.GetPoseWithCovariance ();

		ROSBridgeLib.geometry_msgs.PoseMsg pose = pose_cov.GetPose ();

		PointMsg position = pose.GetPosition ();

		Vector3 pos = new Vector3 (position.GetX (), position.GetZ () + 0.116f, position.GetY ());

		QuaternionMsg orientation = pose.GetOrientation ();

		Quaternion ori = new Quaternion (orientation.GetX(), orientation.GetZ(), orientation.GetY(), orientation.GetW());

		Vector3 rot = new Vector3(ori.eulerAngles.x, ori.eulerAngles.y-90, ori.eulerAngles.z); // y - 90

		GameObject fleet = GameObject.Find ("Fleet");

		FleetController fleetController = fleet.GetComponent<FleetController> ();

		int index = 0;
		int check = 0;
		int id = 0;

		id = int.Parse (number_id);

		while ((index < fleetController.robots.Length) && (fleetController.robots [index].id != 0)) {

			if (fleetController.robots [index].id == id) {

				fleetController.robots [index].position = pos;
				fleetController.robots [index].rotation = rot;
				fleetController.robots [index].time = Time.time;
				fleetController.robots [index].check = 0;
				check = 1;
			}

			index++;
		}

		if (check == 0) {

			fleetController.robots [index].id = id;
			fleetController.robots [index].position = pos;
			fleetController.robots [index].rotation = rot;
			fleetController.robots [index].time = Time.time;
			fleetController.robots [index].check = 0;
		}
	}
}
