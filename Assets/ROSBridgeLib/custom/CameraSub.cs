﻿using ROSBridgeLib;
using ROSBridgeLib.sensor_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;
using System.IO;

/**
 * 
 * Subscriber of image message.
 * 
 * Juan Jesús Roldán: jj.roldan@upm.es
 * 
 **/

public class CameraSub : ROSBridgeSubscriber
{

    public new static string GetMessageTopic()
    {
        return "image";
    }

    public new static string GetMessageType()
    {
        return "sensor_msgs/CompressedImage";
    }

    public new static ROSBridgeMsg ParseMessage(JSONNode msg)
    {
        return new CompressedImageMsg(msg);
    }

    public new static void CallBack(ROSBridgeMsg msg)
    {
        CompressedImageMsg imageMsg = (CompressedImageMsg)msg;

		Debug.Log (msg.ToString ());

        Texture2D tex = new Texture2D(2, 2);
        byte[] image = imageMsg.GetImage();
        tex.LoadImage(image);

		Debug.Log (image.ToString ());

        GameObject canvas = GameObject.Find("HUDCanvas");
        if (canvas != null)
        {
			Texture2D.DestroyImmediate(canvas.GetComponent<Renderer>().material.mainTexture, true);
			canvas.GetComponent<Renderer>().material.mainTexture = tex;
        }

        GameObject cube = GameObject.Find("Cube");
        if (cube != null)
        {
            Texture2D.DestroyImmediate(cube.GetComponent<Renderer>().material.mainTexture, true);
            cube.GetComponent<Renderer>().material.mainTexture = tex;
            cube.transform.Rotate(0, Time.deltaTime * 50, 0, Space.World);
        }
    }
}