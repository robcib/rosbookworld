﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.nav_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/**
 * 
 * Publisher of Odometry message.
 * 
 * Juan Jesús Roldán: jj.roldan@upm.es
 * 
 **/

public class OdometryPub : ROSBridgePublisher {

	public new static string GetMessageTopic() {
		return "/commands";
	}  

	public new static string GetMessageType() {
		return "nav_msgs/Odometry";
	}

	public static string ToString(OdometryMsg msg) {
		return msg.ToString ();
	}

	public static string ToYAMLString(OdometryMsg msg) {
		return msg.ToYAMLString ();
	}
}
