﻿using ROSBridgeLib;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.nav_msgs;
using System.Collections;
using SimpleJSON;
using UnityEngine;

/**
 * 
 * Publisher of robot selection message.
 * 
 * Juan Jesús Roldán: jj.roldan@upm.es
 * 
 **/

public class SelectedRobotPub : ROSBridgePublisher {

	public new static string GetMessageTopic() {
		return "/robot";
	}  

	public new static string GetMessageType() {
		return "std_msgs/Header";
	}

	public static string ToString(HeaderMsg msg) {
		return msg.ToString ();
	}

	public static string ToYAMLString(HeaderMsg msg) {
		return msg.ToYAMLString ();
	}
}
