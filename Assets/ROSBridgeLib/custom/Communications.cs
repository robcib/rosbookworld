﻿using UnityEngine;
using ROSBridgeLib;
using ROSBridgeLib.geometry_msgs;
using ROSBridgeLib.std_msgs;
using ROSBridgeLib.nav_msgs;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Reflection;

/**
 * 
 * Establish communications, defines subscribers and publishers and publish messages.
 * 
 * Juan Jesús Roldán: jj.roldan@upm.es
 * 
 **/

public class Communications : MonoBehaviour {

	private int ind1, ind2;

	private GameObject fleet;

	private FleetController fleetController;

    private ROSBridgeWebSocketConnection ros = null;

    void Start () {

		ros = new ROSBridgeWebSocketConnection ("ws://192.168.141.117", 9090);

		ros.AddSubscriber (typeof(OdometrySub));
		ros.AddSubscriber (typeof(CameraSub));

		ros.AddPublisher (typeof(OdometryPub));
		ros.AddPublisher (typeof(SelectedRobotPub));
			
        ros.Connect();

		ind1 = 0;
		ind2 = 0;
    }

    void OnApplicationQuit()
    {
        if (ros != null)
        {
            ros.Disconnect();
        }
    }

    // Update is called once per frame in Unity
    void Update()
    {
		// Send targets to robots:

		fleet = GameObject.Find ("Fleet");

		fleetController = fleet.GetComponent<FleetController> ();

		Target[] targets = fleetController.targets;

		for (int i = 0; i < targets.Length; i++) {

			if (targets [i].check == 0) {
				
				ind1++;

				TimeMsg time = new TimeMsg ((int)targets [i].time, (int)targets [i].time * 1000000);

				string frame_id;

				if (targets [i].id < 10) {
					frame_id = "ugv_00" + targets [i].id.ToString () + "/odom";
				} else if (targets [i].id < 100) {
					frame_id = "ugv_0" + targets [i].id.ToString () + "/odom";
				} else {
					frame_id = "uav_" + targets [i].id.ToString () + "/world";
				}

				HeaderMsg header = new HeaderMsg (ind1, time, frame_id);

				PointMsg position;

				if (targets [i].id < 100) {
					position = new PointMsg (targets [i].position.x, targets [i].position.z, targets [i].position.y);
				} else {
					position = new PointMsg (targets [i].position.x, targets [i].position.z, targets [i].position.y + 2.0f);
				}

				QuaternionMsg orientation = new QuaternionMsg (Quaternion.Euler (targets [i].rotation).x, Quaternion.Euler (targets [i].rotation).z, Quaternion.Euler (targets [i].rotation).y, Quaternion.Euler (targets [i].rotation).w);

				ROSBridgeLib.geometry_msgs.PoseMsg pose = new ROSBridgeLib.geometry_msgs.PoseMsg (position, orientation);

				double[] cov = new double[36] {0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
					0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
					0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
					0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
					0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 
					0.0, 0.0, 0.0, 0.0, 0.0, 0.0
				};

				PoseWithCovarianceMsg pose_cov = new PoseWithCovarianceMsg (pose, cov);

				Vector3Msg linear = new Vector3Msg (1.0, 0.0, 0.0);
				Vector3Msg angular = new Vector3Msg (0.0, 0.0, 0.0);

				TwistMsg twist = new TwistMsg (linear, angular);

				TwistWithCovarianceMsg twist_cov = new TwistWithCovarianceMsg (twist, cov);

				OdometryMsg cmd = new OdometryMsg (header, frame_id, pose_cov, twist_cov); 

				ros.Publish (OdometryPub.GetMessageTopic (), cmd);

				targets [i] = new Target ();
			}
		}

		// Send ID of selected robot:

		if (fleetController.selectedRobot != null) {
			
			Robot[] robots = fleetController.robots;
			
			for (int i = 0; i < robots.Length; i++) {

				if (robots [i].model == fleetController.selectedRobot) {

					ind2++;

					TimeMsg time = new TimeMsg ((int)targets [i].time, (int)targets [i].time * 1000000);

					string robot_id;

					if (robots [i].id < 10) {
						robot_id = "ugv_00" + robots [i].id.ToString () + "/odom";
					} else if (robots [i].id < 100) {
						robot_id = "ugv_0" + robots [i].id.ToString () + "/odom";
					} else {
						robot_id = "uav_" + robots [i].id.ToString () + "/world";
					}

					HeaderMsg msg = new HeaderMsg (ind2, time, robot_id);
						
					ros.Publish (SelectedRobotPub.GetMessageTopic (), msg);
				}
			}
		} else {

			ind2++;

			TimeMsg time = new TimeMsg ((int)Time.time, (int)Time.time * 1000000);

			string robot_id = "default";

			HeaderMsg msg = new HeaderMsg (ind2, time, robot_id);

			ros.Publish (SelectedRobotPub.GetMessageTopic (), msg);
		}

		ros.Render ();
    }
}