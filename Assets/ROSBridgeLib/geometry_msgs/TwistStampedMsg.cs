using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;


/**
 * 
 * TwistStamped message.
 * 
 * Juan Jesús Roldán: jj.roldan@upm.es
 * 
 **/

namespace ROSBridgeLib {
	namespace geometry_msgs {
		public class TwistStampedMsg : ROSBridgeMsg {
			public HeaderMsg _header;
			public TwistMsg _twist;

			public TwistStampedMsg(JSONNode msg) {
				_header = new HeaderMsg(msg["header"]);
				_twist = new TwistMsg(msg["twist"]);
			}
			
			public TwistStampedMsg(HeaderMsg header, TwistMsg twist) {
				_header = header;
				_twist = twist;
			}

			public static string GetMessageType() {
				return "geometry_msgs/TwistStamped";
			}
			
			public HeaderMsg GetHeader() {
				return _header;
			}

			public TwistMsg GetTwist() {
				return _twist;
			}
			
			public override string ToString() {
				return "TwistStamped [header=" + _header.ToString() + ",  twist=" + _twist.ToString() + "]";
			}
			
			public override string ToYAMLString() {
				return "{\"header\" : " + _header.ToYAMLString() + ", \"twist\" : " + _twist.ToYAMLString() + "}";
			}
		}
	}
}