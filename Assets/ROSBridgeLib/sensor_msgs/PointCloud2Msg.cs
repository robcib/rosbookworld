﻿using System.Collections;
using System.Text;
using SimpleJSON;
using ROSBridgeLib.std_msgs;
using UnityEngine;

/**
 * Define a PointCloud2 message.
 * @author JJRG
 */

namespace ROSBridgeLib {
	namespace sensor_msgs {
		public class PointCloud2Msg : ROSBridgeMsg {
			private HeaderMsg _header;
			private uint _height;
			private uint _width;
			private PointFieldMsg[] _fields;
			private bool _is_bigendian;
			private uint _point_step;
			private uint _row_step;
			private byte[] _data;
			bool _is_dense;

			public PointCloud2Msg(JSONNode msg) {

				//Debug.Log (msg.ToString ());
				_header = new HeaderMsg(msg["header"]);
				_height = uint.Parse(msg["height"]);
				_width = uint.Parse(msg["width"]);

				_fields = new PointFieldMsg[msg ["fields"].Count];
				for (int i = 0; i < _fields.Length; i++) {
					_fields[i] = new PointFieldMsg (msg ["fields"][i]);
				}

				_is_bigendian = bool.Parse (msg ["is_bigendian"]);
				_point_step = uint.Parse (msg ["point_step"]);
				_row_step = uint.Parse (msg ["row_step"]);

				_data = Encoding.ASCII.GetBytes(msg["data"].ToString());

				//Debug.Log (_data.ToString ());

				//Debug.Log ("Data: " + msg ["data"].Count.ToString());

				/*_data = new byte[msg ["data"].Count];
				for (int i = 0; i < _data.Length; i++) {
					_data [i] = byte.Parse(msg ["data"] [i]);
				}*/

				_is_dense = bool.Parse(msg["is_dense"]);
			}

			public PointCloud2Msg(HeaderMsg header, uint height, uint width, PointFieldMsg[] fields, bool is_bigendian, uint point_step, uint row_step, byte[] data, bool is_dense) {
				_header = header;
				_height = height;
				_width = width;
				_fields = fields;
				_is_bigendian = is_bigendian;
				_point_step = point_step;
				_row_step = row_step;
				_data = data;
				_is_dense = is_dense;
			}

			public uint GetHeight() {
				return _height;
			}

			public uint GetWidth() {
				return _width;
			}

			public PointFieldMsg[] GetFields() {
				return _fields;
			}

			public bool GetIsBigendian(){
				return _is_bigendian;
			}

			public uint GetPointStep(){
				return _point_step;
			}

			public uint GetRowStep(){
				return _row_step;
			}

			public byte[] GetData(){
				return _data;
			}

			public bool GetIsDense(){
				return _is_dense;
			}
				
			public static string GetMessageType() {
				return "sensor_msgs/PointCloud2";
			}

			public override string ToString() {
				return "PointCloud2 [header=" + _header.ToString() + ",  height=" + _height.ToString() + ", width=" + _width.ToString() + ", fields=" + _fields[0].ToString() + _fields[1].ToString() + _fields[2].ToString() + ", is_bigendian=" + _is_bigendian.ToString() + ", point_step=" + _point_step.ToString() + ", row_step=" + _row_step.ToString() + ", data=" + _data.ToString() + ", is_dense=" + _is_dense.ToString() + "]";
			}

			public override string ToYAMLString() {
				return "{\"header\" : " + "\"" + _header.ToYAMLString() + "\", \"height\" : \"" + _height + "\", \"width\" : " + _width + "\", \"fields\" : " + _fields[0].ToYAMLString() + _fields[1].ToYAMLString() + _fields[2].ToYAMLString() + "\", \"fields\" : " + _is_bigendian + "\", \"point_step\" : " + _point_step + "\", \"row_step\" : " + _row_step + "\", \"data\" : " + _data + "\", \"is_dense\" : " + _is_dense +"}";
			}
		}
	}
}
