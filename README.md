# ROSBookWorld #

This repository contains the additional material of the following publication:

Roldán, J.J., Peña-Tapia, E., Garzon-Ramos, D., de León, J., Garzón, M., del Cerro, J and Barrientos, A. 
Multi-Robot Systems, Virtual Reality and ROS: Developing a new generation of operator interfaces. 
Robot Operating System, 2018, Springer, Cham.

It contains a scenario with a critical infrastructure where a team of aerial 
and ground robots performs a mission of surveillance. It only contains the
Virtual Reality interface developed in Unity. The ground and aerial robots can
be simulated by using Gazebo.

Visit the Wiki of the repository for more details.